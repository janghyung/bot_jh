
print("collector_jh 프로그램이 시작 되었습니다!")

from library.collector_api_jh import *

class Collector:
    print("collector 클래스에 들어왔습니다.")

    def __init__(self):
        print("__init__ 함수에 들어왔습니다.")
        self.collector_api_jh = collector_api_jh()

    def collecting(self):
        self.collector_api_jh.code_update_check()

if __name__ == "__main__":
    print("__main__에 들어왔습니다.")

    c = Collector()
    # 데이터 수집 시작 -> 주식 종목, 종목별 금융 데이터 모두 데이터베이스에 저장.
    c.collecting()

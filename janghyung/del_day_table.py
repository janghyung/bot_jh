
from sqlalchemy import create_engine
import pymysql

pymysql.install_as_MySQLdb()

class del_table():
    print("del_table 클래스에 들어왔습니다.")

    def __init__(self):
        print("__init__ 함수에 들어왔습니다.")
        # 삭제할 기준일자 설정
        self.del_day = input("삭제할 기준 일자를 선택하세요. YYYYMMDD: ")
        if len(self.del_day) != 8:
            exit(1)

        self.db_setting()
        self.del_tab(self.del_day)

    def db_setting(self):
        # db 계정
        self.db_id = 'bot'  # [mysql ID를 넣어주세요]
        # db ip
        self.db_ip = 'localhost'
        # db 패스워드
        self.db_passwd = 'qlalfqjs12'  # [mysql password 를 넣어주세요]
        # db port가 3306이 아닌 다른 port를 사용 하시는 분은 아래 변수에 포트에 맞게 수정하셔야합니다.
        self.db_port = '3306'

        self.engine_daily_craw = create_engine(
            "mysql+mysqldb://" + self.db_id + ":" + self.db_passwd + "@" + self.db_ip + ":" + self.db_port + "/daily_craw",
            encoding='utf-8')
        self.engine_daily_buy_list = create_engine(
            "mysql+mysqldb://" + self.db_id + ":" + self.db_passwd + "@" + self.db_ip + ":" + self.db_port + "/daily_buy_list",
            encoding='utf-8')

    def tab_sel(self, db_name, date):
        print("tab_sel 함수에 들어왔습니다.")
        if len(date) != 8:
            date = '%%'
            dc_sql = "select TABLE_NAME from information_schema.tables where table_schema = '%s' and TABLE_NAME like '%s'"
        else:
            dc_sql = "select TABLE_NAME from information_schema.tables where table_schema = '%s' and TABLE_NAME between '%s' and '99991231' "

        self.rows = self.engine_daily_buy_list.execute(dc_sql % (db_name,  date)).fetchall()
        print("rows : ", self.rows)
        if len(self.rows):
            return True
        else:
            return

    def tab_del_daily_craw(self, db_name, date):
        print("tab_del_daily_craw 함수에 들어왔습니다.")
        for i in range(len(self.rows)):
            table_name = self.rows[i][0]
            print("delete daily_craw." + table_name)
            dc_sql = "delete from `" + table_name + "` where date > '%s' "
            self.engine_daily_craw.execute(dc_sql % (date))
            # print(" dc_sql = " + dc_sql)

    def tab_del_daily_buy_list(self, db_name, date):
        print("tab_del_daily_buy_list 함수에 들어왔습니다.")
        for i in range(len(self.rows)):
            table_name = self.rows[i][0]
            print("drop daily_buy_list." + table_name)
            dc_sql = "drop table `" + table_name + "`"
            self.engine_daily_buy_list.execute(dc_sql % ())


    def del_tab(self, date):
        print("del_tab 함수에 들어왔습니다.")
        self.db_name = 'daily_craw'
        if self.tab_sel(self.db_name, '') :
            self.tab_del_daily_craw(self.db_name, self.del_day)

        self.db_name = 'daily_buy_list'
        if self.tab_sel(self.db_name, self.del_day):
            self.tab_del_daily_buy_list(self.db_name, self.del_day)

if __name__ == "__main__":
    print("__main__에 들어왔습니다.")

    tb_sel = del_table()

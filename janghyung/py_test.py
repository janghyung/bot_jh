import FinanceDataReader as fdr
import pandas as pd
import pymysql
from sqlalchemy import create_engine

pymysql.install_as_MySQLdb()
# import MySQLdb


class Collection():
    def __init__(self):
        stocks_lists = self.stocks_code()

        for i in stocks_lists:
            print(i)
            self.stocks_price(i)

    ##### 상장사 목록수집
    def stocks_code(self):
        # df_krx = fdr.StockListing('KRX')
        self.df_krx = fdr.StockListing('KOSPI')
        return self.df_krx["Symbol"]

    ##### 가격정보(주가정보) 수집 및 DB에 Table을 생성하여 저장
    def stocks_price(self, code):
        # DataReader(symbol, start=None, end=None, exchange=None, data_source=None)
        df = fdr.DataReader(code, '2016-01-01')  # 시작일자 이후 종료일자 넣으면 원하는 일자까지 가지고 올수 있음 디폴트는 최근일자!
        df = pd.DataFrame(df)
        print(df)
        if df.empty == True:
            print('empty code : {}'.format(code))
        # else:
            # engine = create_engine("mysql+mysqldb://root:" + "비번" + "@localhost/DB명", encoding='utf-8')
            # conn = engine.connect()
            # df.to_sql(name=code.lower(), con=engine, if_exists='replace')
            # conn.close()


Collection()



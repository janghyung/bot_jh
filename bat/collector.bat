@Echo off
@Echo collector Start
set x=0
set time_unit=1
set max=5000
set target_window="stock_collector"
set file="%~dp0\..\collector_v3.py"
set activate_path="C:\ProgramData\Anaconda3\Scripts\activate.bat"
IF EXIST %activate_path% (
    call %activate_path% base
) ELSE (
    echo Cannot find Anaconda3 activate.bat in %activate_path%
    pause
    exit 1
)

:repeat
tasklist /fi "imagename eq python.exe" /v /fo:csv | findstr /r /c:".*%target_window%[^,]*$" > nul
IF errorlevel 1 goto 1

:0
@timeout /t %time_unit% /nobreak > nul
echo %x%
IF %x% GEQ %max% (
    echo Killing collectors... && for /f "tokens=2 delims=," %%a in ('^
        tasklist /fi "imagename eq python.exe" /v /fo:csv ^| findstr /r /c:".*%target_window%[^,]*$"^
    ') do taskkill /pid %%a /f && set x=0 && goto repeat
)
set /A "x+=time_unit"
goto repeat

:1
echo Starting a new session...
IF EXIST %file% (
    start %target_window% python %file%
) ELSE (
    echo Cannot find collector_v3.py in %file%
    pause
    exit 1
)
set x=0
goto 0

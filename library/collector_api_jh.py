from collections import OrderedDict


ver = "#version 1.3.14"
print(f"collector_api Version: {ver}")

from library.db_connect import *
import pandas as pd
import FinanceDataReader as fdr
import os

import datetime


# 콜렉팅에 사용되는 메서드를 모아 놓은 클래스
class collector_api_jh():
    def __init__(self):

        self.call_time = datetime.datetime.now()
        self.py_gubun = False

        self.db_name = 'jhbot1_imi1'
        self.db_con = db_connect(self.db_name)
        self.cf = self.db_con.cf
        self.engine_JH = self.db_con.engine_JH
        self.date_setting()
        self.variable_setting()

    # 날짜 세팅
    def date_setting(self):
        self.today = datetime.datetime.today().strftime("%Y%m%d")
        self.today_detail = datetime.datetime.today().strftime("%Y%m%d%H%M")
    # 변수 세팅
    def variable_setting(self):
        self.py_gubun = "collector"
        # self.market_start_time = QTime(9, 0, 0)
        # self.market_start_time = QTime(15, 11, 0)
        # self.market_end_time = QTime(15, 31, 0)
        # self.market_end_time = QTime(23, 12, 0)

    def get_code_list(self):
        # self.get_item()
        self.get_item_kospi()
        self.get_item_kosdaq()
        self.get_item_konex()
        self.get_item_managing()
        self.get_item_insincerity()

        logger.debug("get_code_list")

        # 아래 부분은 영상 촬영 후 좀 더 효율적으로 업그레이드 되었으므로 강의 영상속의 코드와 다를 수 있습니다.
        # OrderedDict를 사용해 순서 보장
        stock_data = OrderedDict(
            kospi=self.code_df_kospi,
            kosdaq=self.code_df_kosdaq,
            konex=self.code_df_konex,
            insincerity=self.code_df_insincerity,
            managing=self.code_df_managing
        )

        for _type, data in stock_data.items():
            stock_data[_type] = self.db_con._stock_to_sql(data, _type)


        # stock_insincerity와 stock_managing의 종목은 따로 중복하여 넣지 않음
        excluded_tables = ['insincerity', 'managing']
        stock_item_all_df = pd.concat(
            [v[v['code_name'].map(len) > 0] for k, v in stock_data.items() if k not in excluded_tables],
            ignore_index=True
        ).drop_duplicates(subset=['code', 'code_name'])
        self.db_con._stock_to_sql(stock_item_all_df, "item_all")

        sql = "UPDATE setting_data SET code_update='%s' limit 1"
        self.engine_JH.execute(sql % (self.today))

    def _get_code_list_by_market(self, market_num):
        codes = self.db_con.dynamicCall(f'GetCodeListByMarket("{market_num}")')
        return codes.split(';')

    def set_daily_crawler_table(self, code, code_name):
        df = self.get_total_data(code, code_name, self.today)
        oldest_row = df.iloc[-1]
        check_row = None

        check_daily_crawler_sql = """
            UPDATE  daily_buy_list.stock_item_all SET check_daily_crawler = '4' WHERE code = '{}'
        """

        if self.db_con.engine_craw.dialect.has_table(self.db_con.engine_daily_craw, "daily_craw_all"):
            check_row = self.db_con.engine_daily_craw.execute(f"""
                SELECT * FROM daily_craw WHERE code = '{code}'  AND  date = '{oldest_row['date']}' LIMIT 1
            """).fetchall()
        else:
            self.db_con.engine_craw.execute(check_daily_crawler_sql.format(code))


        if check_row and check_row[0]['close'] != oldest_row['close']:
            logger.info(f'{code} {code_name}의 액면분할/증자 등의 이유로 수정주가가 달라져서 처음부터 다시 콜렉팅')
            # daily_craw 삭제
            logger.info('daily_craw와 min_craw 삭제 중..')
            commands = [
                f"""delete from daily_craw_all where code = '{code}'""",
                f'DROP TABLE IF EXISTS min_craw.`{code_name}`'
            ]
            print(commands)
            for com in commands:

                self.db_con.engine_JH.execute(com)
            logger.info('삭제 완료')
            df = self.get_total_data(code, code_name, self.today)
            self.engine_JH.execute(check_daily_crawler_sql.format(code))


        check_daily_crawler = self.engine_JH.execute(f"""
            SELECT check_daily_crawler FROM daily_buy_list.stock_item_all WHERE code = '{code}'
        """).fetchall()[0].check_daily_crawler

        df_temp = pd.DataFrame(df,
                            columns=['date', 'check_item', 'code', 'code_name', 'd1_diff_rate', 'close', 'open', 'high',
                                     'low',
                                     'volume', 'clo5', 'clo10', 'clo20', 'clo40', 'clo60', 'clo80',
                                     'clo100', 'clo120', "clo5_diff_rate", "clo10_diff_rate",
                                     "clo20_diff_rate", "clo40_diff_rate", "clo60_diff_rate",
                                     "clo80_diff_rate", "clo100_diff_rate", "clo120_diff_rate",
                                     'yes_clo5', 'yes_clo10', 'yes_clo20', 'yes_clo40', 'yes_clo60', 'yes_clo80',
                                     'yes_clo100', 'yes_clo120',
                                     'vol5', 'vol10', 'vol20', 'vol40', 'vol60', 'vol80',
                                     'vol100', 'vol120'
                                     ])

        df_temp = df_temp.sort_values(by=['date'], ascending=True)
        # df_temp = df_temp[1:]

        df_temp['code'] = code
        df_temp['code_name'] = code_name
        df_temp['d1_diff_rate'] = round(
            (df_temp['close'] - df_temp['close'].shift(1)) / df_temp['close'].shift(1) * 100, 2)

        # 하나씩 추가할때는 append 아니면 replace
        clo5 = df_temp['close'].rolling(window=5).mean()
        clo10 = df_temp['close'].rolling(window=10).mean()
        clo20 = df_temp['close'].rolling(window=20).mean()
        clo40 = df_temp['close'].rolling(window=40).mean()
        clo60 = df_temp['close'].rolling(window=60).mean()
        clo80 = df_temp['close'].rolling(window=80).mean()
        clo100 = df_temp['close'].rolling(window=100).mean()
        clo120 = df_temp['close'].rolling(window=120).mean()
        df_temp['clo5'] = clo5
        df_temp['clo10'] = clo10
        df_temp['clo20'] = clo20
        df_temp['clo40'] = clo40
        df_temp['clo60'] = clo60
        df_temp['clo80'] = clo80
        df_temp['clo100'] = clo100
        df_temp['clo120'] = clo120

        df_temp['clo5_diff_rate'] = round((df_temp['close'] - clo5) / clo5 * 100, 2)
        df_temp['clo10_diff_rate'] = round((df_temp['close'] - clo10) / clo10 * 100, 2)
        df_temp['clo20_diff_rate'] = round((df_temp['close'] - clo20) / clo20 * 100, 2)
        df_temp['clo40_diff_rate'] = round((df_temp['close'] - clo40) / clo40 * 100, 2)
        df_temp['clo60_diff_rate'] = round((df_temp['close'] - clo60) / clo60 * 100, 2)
        df_temp['clo80_diff_rate'] = round((df_temp['close'] - clo80) / clo80 * 100, 2)
        df_temp['clo100_diff_rate'] = round((df_temp['close'] - clo100) / clo100 * 100, 2)
        df_temp['clo120_diff_rate'] = round((df_temp['close'] - clo120) / clo120 * 100, 2)

        df_temp['yes_clo5'] = df_temp['clo5'].shift(1)
        df_temp['yes_clo10'] = df_temp['clo10'].shift(1)
        df_temp['yes_clo20'] = df_temp['clo20'].shift(1)
        df_temp['yes_clo40'] = df_temp['clo40'].shift(1)
        df_temp['yes_clo60'] = df_temp['clo60'].shift(1)
        df_temp['yes_clo80'] = df_temp['clo80'].shift(1)
        df_temp['yes_clo100'] = df_temp['clo100'].shift(1)
        df_temp['yes_clo120'] = df_temp['clo120'].shift(1)

        df_temp['vol5'] = df_temp['volume'].rolling(window=5).mean()
        df_temp['vol10'] = df_temp['volume'].rolling(window=10).mean()
        df_temp['vol20'] = df_temp['volume'].rolling(window=20).mean()
        df_temp['vol40'] = df_temp['volume'].rolling(window=40).mean()
        df_temp['vol60'] = df_temp['volume'].rolling(window=60).mean()
        df_temp['vol80'] = df_temp['volume'].rolling(window=80).mean()
        df_temp['vol100'] = df_temp['volume'].rolling(window=100).mean()
        df_temp['vol120'] = df_temp['volume'].rolling(window=120).mean()

        # 여기 이렇게 추가해야함
        if self.db_con.engine_daily_craw.dialect.has_table(self.db_con.engine_daily_craw, "daily_craw_all"):
            df_temp = df_temp[df_temp.date > self.db_con.get_daily_craw_db_last_date(code_name)]


        if len(df_temp) == 0 and check_daily_crawler != '4':
            logger.debug("이미 daily_craw db의 " + code_name + " 테이블에 콜렉팅 완료 했다! df_temp가 비었다!!")

            # 이렇게 안해주면 아래 프로세스들을 안하고 바로 넘어가기때문에 그만큼 tr 조회 하는 시간이 짧아지고 1초에 5회 이상의 조회를 할 수 가있다 따라서 비었을 경우는 sleep해줘야 안멈춘다
            check_item_gubun = 3
            return check_item_gubun

        df_temp[['close', 'open', 'high', 'low', 'volume', 'clo5', 'clo10', 'clo20', 'clo40', 'clo60',
                 'clo80', 'clo100', 'clo120',
                 'yes_clo5', 'yes_clo10', 'yes_clo20', 'yes_clo40', 'yes_clo60', 'yes_clo80', 'yes_clo100',
                 'yes_clo120',
                 'vol5', 'vol10', 'vol20', 'vol40', 'vol60', 'vol80', 'vol100', 'vol120']] = \
            df_temp[
                ['close', 'open', 'high', 'low', 'volume', 'clo5', 'clo10', 'clo20', 'clo40', 'clo60',
                 'clo80', 'clo100', 'clo120',
                 'yes_clo5', 'yes_clo10', 'yes_clo20', 'yes_clo40', 'yes_clo60', 'yes_clo80', 'yes_clo100',
                 'yes_clo120',
                 'vol5', 'vol10', 'vol20', 'vol40', 'vol60', 'vol80', 'vol100', 'vol120']].fillna(0).astype(int)


        df_temp.to_sql(name="daily_craw_all", con=self.db_con.engine_daily_craw, if_exists='append', index=False)


        # check_daily_crawler 가 4 인 경우는 액면분할, 증자 등으로 인해 daily_buy_list 업데이트를 해야하는 경우
        if check_daily_crawler == '4':
            logger.info(f'daily_craw_all  {code_name} 업데이트 완료 {code}')
            logger.info('daily_buy_list 업데이트 중..')
            dbl_dates = self.db_con.engine_JH.execute("""
                SELECT table_name as tname FROM information_schema.tables 
                WHERE table_schema ='daily_buy_list' AND table_name REGEXP '[0-9]{8}'
            """).fetchall()

            for row in dbl_dates:
                logger.info(f'{code} {code_name} - daily_buy_list.`{row.tname}` 업데이트')
                try:
                    new_data = df_temp[df_temp['date'] == row.tname]
                except IndexError:
                    continue
                self.db_con.engine_JH.execute(f"""
                    DELETE FROM `{row.tname}` WHERE code = {code}
                """)
                new_data.to_sql(name=row.tname, con=self.db_con.engine_JH, if_exists='append')

            logger.info('daily_buy_list 업데이트 완료')

        check_item_gubun = 1
        return check_item_gubun
    # 불성실공시법인 가져오는 함수
    def get_item_insincerity(self):
        print("get_item_insincerity!!")

        self.code_df_insincerity = pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=05', header=0)[0]
        # print(self.code_df_insincerity)

        # 6자리 만들고 앞에 0을 붙인다.
        self.code_df_insincerity.종목코드 = self.code_df_insincerity.종목코드.map('{:06d}'.format)

        # 우리가 필요한 것은 회사명과 종목코드이기 때문에 필요없는 column들은 제외해준다.
        self.code_df_insincerity = self.code_df_insincerity[['회사명', '종목코드']]

        # 한글로된 컬럼명을 영어로 바꿔준다.
        self.code_df_insincerity = self.code_df_insincerity.rename(columns={'회사명': 'code_name', '종목코드': 'code'})
    # 관리 종목을 가져오는 함수
    def get_item_managing(self):
        print("get_item_managing!!")
        self.code_df_managing = pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=01', header=0)[0]  # 종목코드가 6자리이기 때문에 6자리를 맞춰주기 위해 설정해줌

        # 6자리 만들고 앞에 0을 붙인다.strPath --> str(unicode(strPath))
        self.code_df_managing.종목코드 = self.code_df_managing.종목코드.map('{:06d}'.format)

        # 우리가 필요한 것은 회사명과 종목코드이기 때문에 필요없는 column들은 제외해준다.
        self.code_df_managing = self.code_df_managing[['회사명', '종목코드']]

        # 한글로된 컬럼명을 영어로 바꿔준다.
        self.code_df_managing = self.code_df_managing.rename(columns={'회사명': 'code_name', '종목코드': 'code'})
    # 코넥스 종목을 가져오는 함수
    def get_item_konex(self):
        print("get_item_konex!!")
        self.code_df_konex = pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13&marketType=konexMkt',header=0)[0]  # 종목코드가 6자리이기 때문에 6자리를 맞춰주기 위해 설정해줌

        # 6자리 만들고 앞에 0을 붙인다.
        self.code_df_konex.종목코드 = self.code_df_konex.종목코드.map('{:06d}'.format)

        # 우리가 필요한 것은 회사명과 종목코드이기 때문에 필요없는 column들은 제외해준다.
        self.code_df_konex = self.code_df_konex[['회사명', '종목코드']]

        # 한글로된 컬럼명을 영어로 바꿔준다.
        self.code_df_konex = self.code_df_konex.rename(columns={'회사명': 'code_name', '종목코드': 'code'})
    # 코스피 종목을 가져오는 함수
    def get_item_kospi(self):
        print("get_item_kospi!!")
        self.code_df_kospi = \
        pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13&marketType=stockMkt',header=0)[0]  # 종목코드가 6자리이기 때문에 6자리를 맞춰주기 위해 설정해줌

        # 6자리 만들고 앞에 0을 붙인다.
        self.code_df_kospi.종목코드 = self.code_df_kospi.종목코드.map('{:06d}'.format)

        # 우리가 필요한 것은 회사명과 종목코드이기 때문에 필요없는 column들은 제외해준다.
        self.code_df_kospi = self.code_df_kospi[['회사명', '종목코드']]

        # 한글로된 컬럼명을 영어로 바꿔준다.
        self.code_df_kospi = self.code_df_kospi.rename(columns={'회사명': 'code_name', '종목코드': 'code'})
    # 코스닥 종목을 가져오는 함수
    def get_item_kosdaq(self):
        print("get_item_kosdaq!!")
        self.code_df_kosdaq = pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13&marketType=kosdaqMkt',header=0)[0]  # 종목코드가 6자리이기 때문에 6자리를 맞춰주기 위해 설정해줌

        # 6자리 만들고 앞에 0을 붙인다.
        self.code_df_kosdaq.종목코드 = self.code_df_kosdaq.종목코드.map('{:06d}'.format)

        # 우리가 필요한 것은 회사명과 종목코드이기 때문에 필요없는 column들은 제외해준다.
        self.code_df_kosdaq = self.code_df_kosdaq[['회사명', '종목코드']]

        # 한글로된 컬럼명을 영어로 바꿔준다.
        self.code_df_kosdaq = self.code_df_kosdaq.rename(columns={'회사명': 'code_name', '종목코드': 'code'})
    # 코스피, 코스닥, 코넥스 모든 정보를 가져오는 함수
    def get_item(self):
        # print("get_item!!")
        self.code_df = pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13', header=0)[0]  # 종목코드가 6자리이기 때문에 6자리를 맞춰주기 위해 설정해줌
        # 6자리 만들고 앞에 0을 붙인다.

        self.code_df.종목코드 = self.code_df.종목코드.map('{:06d}'.format)

        # 우리가 필요한 것은 회사명과 종목코드이기 때문에 필요없는 column들은 제외해준다.
        self.code_df = self.code_df[['회사명', '종목코드']]

        # 한글로된 컬럼명을 영어로 바꿔준다.
        self.code_df = self.code_df.rename(columns={'회사명': 'code_name', '종목코드': 'code'})

    def change_format(self, data):
        strip_data = data.replace('.', '')

        return strip_data
    
    # 콜렉팅을 실행하는 함수
    def code_update_check(self):
        logger.debug("code_update_check 함수에 들어왔습니다.")
        sql = "select code_update,jango_data_db_check, possessed_item, today_profit, final_chegyul_check, db_to_buy_list,today_buy_list, daily_crawler , min_crawler, daily_buy_list from setting_data limit 1"

        rows = self.engine_JH.execute(sql).fetchall()

        # stock_item_all(kospi,kosdaq,konex)
        # kospi(stock_kospi), kosdaq(stock_kosdaq), konex(stock_konex)
        # 관리종목(stock_managing), 불성실법인종목(stock_insincerity) 업데이트
        if rows[0][0] != self.today:
            self.get_code_list()
            
        # daily_craw db 업데이트
        if rows[0][7] != self.today:
            self.daily_crawler_check()

        # daily_buy_list db 업데이트
        if rows[0][9] != self.today:
            self.daily_buy_list_check()

        logger.debug("collecting 작업을 모두 정상적으로 마쳤습니다.")

        # cmd 콘솔창 종료
        os.system("@taskkill /f /im cmd.exe")

    # get_total_data : 특정 종목의 일자별 거래 데이터 조회 함수
    # 사용방법
    # code: 종목코드(ex. '005930' )
    # date : 기준일자. (ex. '20200424') => 20200424 일자 까지의 모든 open, high, low, close, volume 데이터 출력
    def get_total_data(self, code, code_name, date):
        logger.debug("get_total_data 함수에 들어왔다!")

        # DataReader(symbol, start=None, end=None, exchange=None, data_source=None)
        # 시작일자 이후 종료일자 넣으면 원하는 일자까지 가지고 올수 있음 디폴트는 최근일자!
        df = fdr.DataReader(code, self.cf.start_daily_buy_list)
        df = df[['Open', 'High', 'Low', 'Close', 'Volume']]
        df = df.rename(columns={'Open': 'open', 'High': 'high', 'Low': 'low', 'Close': 'close'
                                            , 'Volume': 'volume'})

        df['date'] = (df.index).strftime("%Y%m%d")
        df['date'] = df['date'].replace('-','')

        df = df.reindex(columns=['date', 'open', 'high', 'low', 'close', 'volume'])


        # data 비어있는 경우
        if len(df) == 0:
            return []
        return df

    def daily_crawler_check(self):
        self.db_to_daily_craw()
        logger.debug("daily_crawler success !!!")

        sql = "UPDATE setting_data SET daily_crawler='%s' limit 1"
        self.engine_JH.execute(sql % (self.today))

    def db_to_daily_craw(self):
        logger.debug("db_to_daily_craw 함수에 들어왔습니다!")
        sql = "select code,code_name, check_daily_crawler from stock_item_all"

        # 데이타 Fetch
        # rows 는 list안에 튜플이 있는 [()] 형태로 받아온다

        target_code = self.db_con.engine_daily_buy_list.execute(sql).fetchall()
        num = len(target_code)
        # mark = ".KS"
        sql = "UPDATE stock_item_all SET check_daily_crawler='%s' WHERE code='%s'"

        for i in range(num):
            # check_daily_crawler 확인 후 1, 3이 아닌 경우만 업데이트
            # (1: 금일 콜렉팅 완료, 3:과거에 이미 콜렉팅 완료, 0: 콜렉팅 전, 4: 액면분할, 증자 등으로 인한 업데이트 필요)
            if int(target_code[i][2]) in (1, 3):
                continue

            code = target_code[i][0]
            code_name = target_code[i][1]

            logger.debug("++++++++++++++" + str(code_name) + "++++++++++++++++++++" + str(i + 1) + '/' + str(num))

            check_item_gubun = self.set_daily_crawler_table(code, code_name)

            self.db_con.engine_daily_buy_list.execute(sql % (check_item_gubun, code))